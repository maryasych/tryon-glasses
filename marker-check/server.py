#!/usr/env python3
import http.server
import socketserver
import io
import cgi
import ssl
import json
import os
import markercheck

with open('./marker-check/server-config.json') as json_data_file:
    appConfig = json.load(json_data_file)

try:
    with open('./marker-check/server-config-local.json') as json_data_file:
        appConfigLocal = json.load(json_data_file)
        appConfig.update(appConfigLocal)
except:
    #nothing
    print("no local config.")

print(appConfig)

PORT = appConfig['port']
KEYFILE = appConfig['https']['keyfile']
CERTFILE = appConfig['https']['certfile']

WEB_DIRECTORY = "./www/"

class CustomHTTPRequestHandler(http.server.SimpleHTTPRequestHandler):

    def end_headers(self):
        self.send_header("Access-Control-Allow-Credentials", "true")
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
        self.send_header("Access-Control-Allow-Headers", "Content-Type,X-Requested-With")
        http.server.SimpleHTTPRequestHandler.end_headers(self)

    def do_POST(self):
        r, info, form = self.deal_post_data()
        print(r, info, "by: ", self.client_address)
        bestMatch = None

        if r and form:
            markerName = form.getfirst("marker")
            print("marker:%s"%markerName)
            t = "./%s"%form["file"].filename
            if (markerName):
                print("test marker")
                bestMatch = markercheck.detectMatch(t, markerName)
            else:
                print("autodetect marker")
                bestMatch = markercheck.detectBestMatch(t)
            
        
        print("send response")
        f = io.BytesIO()

        if bestMatch is not None and bestMatch["points"] > 0:
            f.write(str.encode(json.dumps({ "ok":True, "width":bestMatch["w"], "height":bestMatch["h"], "features":bestMatch["features"], "matches":bestMatch["points"], "img":bestMatch["resultImage"], "marker":bestMatch['markerFile'] })))
        else:
            f.write(str.encode(json.dumps({ "ok":False })))

        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(length))

        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()      

    def deal_post_data(self):
        print("deal_post_data")
        ctype, pdict = cgi.parse_header(self.headers['Content-Type'])
        pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
        pdict['CONTENT-LENGTH'] = int(self.headers['Content-Length'])
        if ctype == 'multipart/form-data':
            print("--multipart_post_data")
            form = cgi.FieldStorage( fp=self.rfile, headers=self.headers, environ={'REQUEST_METHOD':'POST', 'CONTENT_TYPE':self.headers['Content-Type'], })
            print (type(form))
            try:
                if isinstance(form["file"], list):
                    for record in form["file"]:
                        open("./%s"%record.filename, "wb").write(record.file.read())
                else:
                    open("./%s"%form["file"].filename, "wb").write(form["file"].file.read())
            except IOError:
                    return (False, "Can't create file to write, do you have permission to write?")
            return (True, "Ok", form)

        print("--!!!-- No post data")
        return (False, "No post data", None)

os.chdir(WEB_DIRECTORY)

Handler = CustomHTTPRequestHandler
with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)

    httpd.socket = ssl.wrap_socket (httpd.socket,
        keyfile=KEYFILE, 
        certfile=CERTFILE, server_side=True)

    #httpd.socket = ssl.wrap_socket (httpd.socket, certfile='c:/_WORK/_www/face-api.loc/marker-check/server.crt', server_side=True)

    httpd.serve_forever()
